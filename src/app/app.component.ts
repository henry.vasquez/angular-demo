import { Component } from '@angular/core';
import { MagnoliaService } from "./magnolia.service"
import { HttpClient } from "@angular/common/http"
import { DomSanitizer } from "@angular/platform-browser"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-demo';
  html: any = ''

  constructor(private magnoliaService: MagnoliaService, private http: HttpClient, private sanitized: DomSanitizer) {
    // this.data = this.magnoliaService.getData();
  }

  ngOnInit() {
    this.http.get<any>("https://fisdemo.nrg-edge.com/magnoliaAuthor/.rest/interstitialMessages/page?page=interstitial-messages/RBC-Page").subscribe(d => {
      // this.html = d[0]
      this.html = this.sanitized.bypassSecurityTrustHtml(d[0])
      console.log(d[0])
    })
  }
}
