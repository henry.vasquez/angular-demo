import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MagnoliaService {

  constructor() { }

  getData(): any[] {
    return [
      {
        title: "something"
      },
      {
        title: "another title"
      }
    ];
  }
}
