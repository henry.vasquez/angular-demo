import { TestBed } from '@angular/core/testing';

import { MagnoliaService } from './magnolia.service';

describe('MagnoliaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MagnoliaService = TestBed.get(MagnoliaService);
    expect(service).toBeTruthy();
  });
});
